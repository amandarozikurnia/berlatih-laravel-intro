<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        $first = $request->first_nama;
        $last = $request->last_nama;

        return view('welcome', compact('first', 'last'));
    }
}